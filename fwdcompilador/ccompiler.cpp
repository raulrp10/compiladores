#include "ccompiler.h"

string n1 = "0", n2 = "0";
CCompiler::CCompiler(void) {
	_mStopWords = "(){}[]+-*/%&|;\n\t, =><";
	_mAlphabeth = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";

	_mStdin.open("lexico");
	_mStdout.open("sintactico");
	_mStderr.open("errores");
	}

void CCompiler::__print_sintactic_analisis (void){
	_mStdout << "lista de tokens" << endl;
	container_t::iterator it = _mTokenL.begin();
	for (; it != _mTokenL.end(); it++)
		_mStdout << it->_mToken << " ";
	_mStdout << endl;

	_mStdout << "pila sintactica" << endl;
	list<string>::reverse_iterator _it = _mSintacticStack.rbegin();
	for (; _it != _mSintacticStack.rend(); _it++)
		_mStdout << *_it << " ";
	_mStdout << endl;
	_mStdout << endl;
}

CCompiler::container_t::iterator CCompiler::__find (string _s, CCompiler::container_t& _l){
	container_t::iterator it = _l.begin();
	for (; it != _l.end(); it++)
		if (_s == it->_mLexema) break;
	return it;
}

CLexema last_id, last_op;

bool CCompiler::sintactic_analisis (string _table){
	bool _status  = true;
	__load_sintactic_table(_table);

	_mSintacticStack.push_front("$");
	_mSintacticStack.push_front("CUERPO");
	CLexema _lexema ("$");
	_lexema._mToken = "$";
	_mTokenL.push_back(_lexema);

	__mTokenL = _mTokenL;

	__print_sintactic_analisis ();

	while (!_mSintacticStack.empty()){
		if (_mSintacticStack.front() == "e") {
			_mSintacticStack.pop_front();
			__print_sintactic_analisis ();
		} else {
			if (*(_mSintacticStack.begin()) == _mTokenL.begin()->_mToken){
				_mSintacticStack.pop_front();
				_mTokenL.pop_front();
			} else {
				int i = 0, j = 0;
				for (; i < _mSintacticTable[0].size(); i++)
					if (_mSintacticTable[0][i] == _mTokenL.begin()->_mToken)
						break;
				for (; j < _mSintacticTable.size(); j++)
					if (_mSintacticTable[j][0] == *(_mSintacticStack.begin()))
						break;
				string production = _mSintacticTable[j][i];
				if (production != "*") {
					string word = "";
					list<string> words;
					for (int i = 0; i < production.size(); i++){
						if (production[i] != ' ') word += production[i];
						else {
							words.push_back(word);
							word.clear();
						}
					}
					_mSintacticStack.pop_front();
					words.push_back(word);
					list<string>::reverse_iterator rit = words.rbegin();
					for (; rit != words.rend(); rit++)
						_mSintacticStack.push_front(*rit);
				} else {
					container_t::iterator it = _mTokenL.begin(); it++;
					_mStderr	<< SINT_ERR_I << " in line: " << _mTokenL.begin()->_mLine
								<< " symbol " << _mTokenL.begin()->_mLexema
								<< " near to " << it->_mLexema << endl;
					_status = false;

					string top = _mSintacticStack.front();
					_mSintacticStack.pop_front();
					break;
				}
			}
			__print_sintactic_analisis ();
		}
	}
	return _status;
}

void CCompiler::__load_sintactic_table (string f){
	ifstream _file(f.c_str());
	_mSintacticTable.clear();

	while (!_file.eof()){
		string _line;
		getline(_file,_line);
		char *line = new char [_line.size()];
		strcpy(line,_line.c_str());
		char * toks = strtok(line, ".");

		vector< string > _row;
		while (toks != NULL){
			_row.push_back(toks);
			toks = strtok (NULL, ".");
		}
		_mSintacticTable.push_back(_row);
	}
	for (int i = 0; i < _mSintacticTable.size(); i++){
		for(int k = 0; k < _mSintacticTable[i].size(); k++){
				_mStdout << _mSintacticTable[i][k] << " ";
		}
		_mStdout << endl;
	}
}


void CCompiler::lexic_analisis (string _file){
	 __preprocesing(_file);
	container_t::iterator itx = _mTokenL.begin();
	itx = _mTokenL.begin();
	container_t::iterator itxt;
	for (;itx != _mTokenL.end(); itx++){
		if (itx->_mLexema == "="){
			itxt = itx;
			itxt ++;
			if (itxt->_mLexema == "="){
				itx->_mLexema = "==";
				itx->_mToken = __tokenize(*itx);
				_mTokenL.erase(itxt);
			}
		}
	}

	_mStdin << "Lista de tokens" << endl << endl;
	itx = _mTokenL.begin();
	for (;itx != _mTokenL.end(); itx++)
		_mStdin << *itx << endl;

	itx = _mTokenL.begin();
	for (;itx != _mTokenL.end(); itx++){
		if (itx->_mToken == ID){
			if (__find(itx->_mLexema,_mSymbolT) == _mSymbolT.end())
			_mSymbolT.push_back(*itx);
		}
	}

	_mStdin << "\n\nTabla de simbolos" << endl << endl;
	container_t::iterator _itx = _mSymbolT.begin();
	for (;_itx != _mSymbolT.end(); _itx++)
		_mStdin << *_itx << endl;
	}


void CCompiler::__preprocesing(string _file) {
	ifstream file_(_file.c_str());
	string line_;
	int cline_ = 0;
	int cscope_ = 0;
	int comment_ = 0;
	bool dcomment_ = false;

	while (!file_.eof()){
		cline_ ++;
		getline(file_,line_);
		line_.push_back('\n');
		string tword_;

		for(string::iterator sit = line_.begin(); sit != line_.end(); sit++){
			if(_mStopWords.find(*sit) == string::npos){
				tword_.push_back(*sit);
			}
			else{
				if (*sit == '/') comment_++;
				if (comment_ == 2) {
					_mTokenL.pop_back();
					comment_ = 0;
					break;
				}

				if (*sit == '}') cscope_ ++;
				if (*sit == '{') cscope_ --;
				if (!tword_.empty()){
					CLexema lexema_ (tword_);
					lexema_._mLine = cline_;
					lexema_._mScope = cscope_;
					lexema_._mToken = __tokenize(lexema_);
					_mTokenL.push_back(lexema_);
					}

				tword_.clear();
				string spaces_ = " \t\n";
				if (spaces_.find(*sit) == string::npos){
					tword_ = *sit;
					CLexema separator_ (tword_);
					separator_._mLine = cline_;
					separator_._mScope = cscope_;
					separator_._mToken = __tokenize(separator_);
					_mTokenL.push_back(separator_);
					tword_.clear();
					}
				}
			}
		line_.clear();
		}
	}


string CCompiler::__tokenize(CLexema & _lex) {
	if (_lex._mLexema == "bool"	|
		_lex._mLexema == "int"		|
		_lex._mLexema == "float"	|
		_lex._mLexema == "double"	|
		_lex._mLexema == "string"	|
		_lex._mLexema == "char"	|
		_lex._mLexema == "void"
		)		return TYPE;

	if (_lex._mLexema == "+"	|
		_lex._mLexema == "-"	|
		_lex._mLexema == "/"	|
		_lex._mLexema == "*"	|
		_lex._mLexema == "%"
		)		return A_OPERATOR;

	if (_lex._mLexema == "<"	|
		_lex._mLexema == ">"	|
		_lex._mLexema == ">="	|
		_lex._mLexema == "<="	|
		_lex._mLexema == "=="
		)		return C_OPERATOR;

	if (_lex._mLexema == "=") return  ASSIG;
	if (_lex._mLexema == "if") return IF;
	if (_lex._mLexema == "else") return ELSE;
	if (_lex._mLexema == "for") return FOR;
	if (_lex._mLexema == "while") return WHILE;
	if (_lex._mLexema == "declare") return DECLARE;
	if (_lex._mLexema == "elseif") return ELSEIF;
	if (_lex._mLexema == "main") return MAIN;

	if (_lex._mLexema == "(") return PA;
	if (_lex._mLexema == ")") return PC;
	if (_lex._mLexema == "{") return LA;
	if (_lex._mLexema == "}") return LC;
	if (_lex._mLexema == "[") return CA;
	if (_lex._mLexema == "]") return CC;

	if (_lex._mLexema == ",") return COMMA;
	if (_lex._mLexema == ":") return DOTDOT;
	if (_lex._mLexema == ";") return DOTCOMMA;

	if (_lex._mLexema == ";") return ENDL;
	if (*(_lex._mLexema.begin()) == '"' && *(_lex._mLexema.rbegin()) == '"') return CONSTANT;

	string nums_ = "0123456789";
	if (nums_.find(_lex._mLexema[0]) != string::npos){
		for (int i = 0; i <  _lex._mLexema.size(); i++){
			if (_lex._mLexema[i] == '.' && (_lex._mLexema.find('.')==_lex._mLexema.find_last_of('.'))){
				for (int k = i+1; k <  _lex._mLexema.size(); k++){
					if (nums_.find(_lex._mLexema[k]) == string::npos)
						_mStderr << LEXIC_ERR_I << " en la linea : " << _lex._mLine << " simbolo " << _lex._mLexema << endl;
					}
				return CONSTANT;
				}
				else if(_lex._mLexema.find('.')!=_lex._mLexema.find_last_of('.'))
				{
					_mStderr << LEXIC_ERR_I << " en la linea : " << _lex._mLine << " simbolo " << _lex._mLexema << endl;
					return LEXIC_ERR_I;
				}
			if (nums_.find(_lex._mLexema[i]) == string::npos)
				_mStderr << LEXIC_ERR_I << " en la linea: " << _lex._mLine << " simbolo " << _lex._mLexema  << endl;
			}
		return CONSTANT;
		}
	for (int i = 1; i < _lex._mLexema.size();  i++){
		if (_mAlphabeth.find(_lex._mLexema[i]) == string::npos){
			_mStderr << LEXIC_ERR_I << " en la linea: " << _lex._mLine << " simbolo " << _lex._mLexema  << endl;
		}
	}	return ID;
}
