#ifndef NN_SYMBOL
#define NN_SYMBOL

#include <string>
#include <iostream>
#include <fstream>
using namespace std;

class NNSymbol {
public:
	string	_name;
	string	_type;
	void*	_val;
	void ** _matrix;

	NNSymbol	(string _n)
	: _name (_n){}

	~NNSymbol	(void){}

};

#endif //