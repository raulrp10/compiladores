#ifndef CLEXEMA_H
#define CLEXEMA_H

#include <string>
#include <list>
#include <iostream>
using namespace std;

#include "Macros.h"

class CLexema
	{
	public:
	string	_mLexema;
	string	_mToken;
	int		_mScope;
	int		_mLine;
	int		_mSizeOf;
	void*	_mValue;
	string  _mType;
	string	_mVal;
	string	_mMemoryDir;
	list<string> _mDimension;
	list<string> _mPosition;
	void ** _matrix;


	public:
	CLexema	(const string = "NULL");
	CLexema	(const string, int);

	void addcol (string);
	void addrow (string);

	friend ostream& operator << (ostream&,const CLexema&);

	friend bool operator < (const CLexema& _lex, const CLexema& __lex){
		return _lex._mLexema < __lex._mLexema;
	}
};


#endif // CLEXEMA_H
