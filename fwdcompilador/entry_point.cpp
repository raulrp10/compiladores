#include <iostream>
#include <fstream>
#include <string>
#include <stack>
using namespace std;

#include "clexema.h"
#include "ccompiler.h"


int main (int argc, char * argv[]){
	CCompiler _c;
	cout << "Procediendo a analizar lexicamente ..." << endl;
	_c.lexic_analisis("main");
	if (_c.sintactic_analisis ("tabla2.csv")) {
		cout << "Sin errores" << endl;
	}
	else cout << "Errores encontrados en sintaxis" << endl;
//	return true;
	}
