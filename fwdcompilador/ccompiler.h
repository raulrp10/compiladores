#ifndef CCOMPILER_H
#define CCOMPILER_H

#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include <queue>
#include <list>
#include <set>
using namespace std;

#include "clexema.h"
#include "Macros.h"
#include "NNSymbol.h"

class CCompiler
	{
	public:
	typedef list<CLexema>  container_t;
	typedef set<CLexema>	symbol_t;
	typedef vector< vector <string> > sintactic_table_t;
	typedef list<string> sintactic_stack_t;
	typedef list<CLexema> semantic_stack_t;

	protected:
	string _mStopWords;
	string _mAlphabeth;
	sintactic_table_t	_mSintacticTable;
	sintactic_stack_t	_mSintacticStack;
	semantic_stack_t	_mSemanticStack;

	container_t		_nnSymbols;
	container_t		_mSymbolT;	// tabla de simbolos
	container_t _mTokenL;	// lista de tokens
	container_t __mTokenL;	// lista de tokens para el esquema
	container_t _mErrorL;	// lista de errores

	// Archivos de salida standard
	ofstream _mStdin, _mStdout, _mStderr;

	public:
	CCompiler(void);


	bool sintactic_analisis (string _table);
	void lexic_analisis (string);
	void semantic_analisis(string);

	private:
	string  __tokenize			(CLexema&);	// Lexico
	void __preprocesing			(string);	// Preprocesing
	void __print_sintactic_analisis (void);
	void __load_sintactic_table (string);
	void __execute (string, container_t::iterator&, CLexema&);
	container_t::iterator __find (string, container_t&);
};

#endif // CCOMPILER_H
