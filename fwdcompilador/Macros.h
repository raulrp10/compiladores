#ifndef MACROS_H
#define MACROS_H

// Palabras Clave
#define FOR		"for"
#define WHILE	"while"
#define IF		"if"
#define ELSE	"else"
#define ELSEIF "elseif"
#define DECLARE "declare"
#define ID		"id"
#define PA		"("
#define PC		")"
#define CA		"["
#define CC		"]"
#define LA		"{"
#define LC		"}"
#define COMMA	","
#define DOTDOT	":"
#define DOTCOMMA ";"
#define TYPE	 "tipo"
#define CONSTANT "constante"
#define A_OPERATOR	"op"	// + * / -
#define C_OPERATOR	"cmp"	// < > == >= <=
#define ASSIG		"="		// =
#define MAIN "main"

// Separadores
#define ENDL		"endl"		//;
#define SEPARATOR	"separator"		//;

// Errores
#define LEXIC_ERR_I		"Simbolo no encontrado"	// Error lexico
#define SINT_ERR_I		"Error de sintaxis"		// Error lexico

#endif // MACROS_H
