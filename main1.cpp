#include <iostream>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <sstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>

using namespace std;

vector<string> variable;
vector<string> numero;
vector<string> tipo_dato;
vector<string> bucles;
vector<string> condicionales;
vector<string> op_logicos;
vector<string> op_artimeticos;
vector<string> comentarios;
vector<string> simbolos;
vector<string> errores;
vector<string> llaves;
vector<string> PR;
string ** T_S;
vector <string> Variable_S;
vector <string> T_SE;


void llenar_datos(){
	variable.push_back("a");
	variable.push_back("b");
	variable.push_back("c");
	variable.push_back("d");
	variable.push_back("e");
	variable.push_back("f");
	variable.push_back("g");
	variable.push_back("h");
	variable.push_back("i");
	variable.push_back("j");
	variable.push_back("k");
	variable.push_back("l");
	variable.push_back("m");
	variable.push_back("n");
	variable.push_back("o");
	variable.push_back("p");
	variable.push_back("q");
	variable.push_back("r");
	variable.push_back("s");
	variable.push_back("t");
	variable.push_back("u");
	variable.push_back("v");
	variable.push_back("w");
	variable.push_back("x");
	variable.push_back("y");
	variable.push_back("z");
	variable.push_back("_");
	variable.push_back("?");

	numero.push_back("0");
	numero.push_back("1");
	numero.push_back("2");
	numero.push_back("3");
	numero.push_back("4");
	numero.push_back("5");
	numero.push_back("6");
	numero.push_back("7");
	numero.push_back("8");
	numero.push_back("9");
	numero.push_back(".");

	tipo_dato.push_back("int");
	tipo_dato.push_back("char");
	tipo_dato.push_back("float");
	tipo_dato.push_back("vacio");
	tipo_dato.push_back("bool");

	bucles.push_back("for");
	bucles.push_back("while");

	condicionales.push_back("if");
	condicionales.push_back("else");
	condicionales.push_back("elif");

	op_logicos.push_back(">");
	op_logicos.push_back("<");
	op_logicos.push_back("<=");
	op_logicos.push_back(">=");
	op_logicos.push_back("==");
	op_logicos.push_back("!=");
	op_logicos.push_back("&&");
	op_logicos.push_back("||");
	op_logicos.push_back("&");
	op_logicos.push_back("|");
	op_logicos.push_back("!");

	op_artimeticos.push_back("+");
	op_artimeticos.push_back("-");
	op_artimeticos.push_back("*");
	op_artimeticos.push_back("/");
	op_artimeticos.push_back("%");

	comentarios.push_back("/*");
	comentarios.push_back("*/");
	comentarios.push_back("//");

	llaves.push_back("(");
	llaves.push_back(")");
	llaves.push_back("{");
	llaves.push_back("}");

	simbolos.push_back("=");
	simbolos.push_back(",");
	simbolos.push_back(";");
	simbolos.push_back(":");

	PR.push_back("fun");
	PR.push_back("nulo");
	PR.push_back("print");
	PR.push_back("return");
	PR.push_back(":=");
	PR.push_back(":");
	PR.push_back("main");
}

string **crear_matrix(int filas,int columnas){
    int f=filas;
    int c=columnas;
    string **matrix = new string*[f];
    for(int i=0;i<f;i++){
        matrix[i]=new string[c];
        for(int a=0;a<c;a++){
			//string h;
			//cin >> h;
            matrix[i][a]=" ";
        }
    }
    return matrix;
}

void mostrar_matrix(string **matrix,int f,int c){
    for(int i=0;i<f;i++){
        for(int a=0;a<c;a++){
            cout << matrix[i][a];
            cout << '\t';
        }
        cout << endl;
    }
}

void llenar_TS(){
	T_S = crear_matrix(25,40);
	T_S[1][0] = "R";
	T_S[2][0] = "TF";
	T_S[3][0] = "F";
	T_S[4][0] = "C";
	T_S[5][0] = "PE";
	T_S[6][0] = "I";
	T_S[7][0] = "A";
	T_S[8][0] = "D";
	T_S[9][0] = "NV";
	T_S[10][0] = "Y";
	T_S[11][0] = "V";
	T_S[12][0] = "B";
	T_S[13][0] = "PA";
	T_S[14][0] = "M";
	T_S[15][0] = "CO";
	T_S[16][0] = "S";
	T_S[17][0] = "SN";
	T_S[18][0] = "E";
	T_S[19][0] = "X";
	T_S[20][0] = "CD";
	T_S[21][0] = "OA";
	T_S[22][0] = "OL";
	T_S[23][0] = "TD";
	T_S[24][0] = "RE";
	T_S[0][1] = "return";
	T_S[0][2] = "fun";
	T_S[0][3] = "main";
	T_S[0][4] = "variable";
	T_S[0][5] = "int";
	T_S[0][6] = "float";
	T_S[0][7] = "vacio";
	T_S[0][8] = "char";
	T_S[0][9] = "bool";
	T_S[0][10] = "valor_entero";
	T_S[0][11] = "valor_flotante";
	T_S[0][12] = "valor_caracter";
	T_S[0][13] = "valor_buleano";
	T_S[0][14] = "for";
	T_S[0][15] = "while";
	T_S[0][16] = "if";
	T_S[0][17] = "elif";
	T_S[0][18] = "else";
	T_S[0][19] = "+";
	T_S[0][20] = "-";
	T_S[0][21] = "*";
	T_S[0][22] = "/";
	T_S[0][23] = "%";
	T_S[0][24] = "<";
	T_S[0][25] = ">";
	T_S[0][26] = "<=";
	T_S[0][27] = ">=";
	T_S[0][28] = "==";
	T_S[0][29] = "!=";
	T_S[0][30] = "&&";
	T_S[0][31] = "||";
	T_S[0][32] = "(";
	T_S[0][33] = ")";
	T_S[0][34] = "{";
	T_S[0][35] = "}";
	T_S[0][36] = ",";
	T_S[0][37] = ":=";
	T_S[0][38] = ";";
	T_S[0][39] = "$";
	T_S[4][1] = "RE";
	T_S[24][1] = "; X return";
	T_S[1][2] = "R F";
	T_S[3][2] = "} C { ) PE ( TF fun";
	T_S[10][2] = ") PE ( TF fun";
	T_S[3][10] = ") PE ( TF fun";
	T_S[2][3] = "main";
	T_S[1][4] = "R I";
	T_S[2][4] = "NV";
	T_S[4][4] = "C I";
	T_S[5][4] = "Y NV";
	T_S[6][4] = "I NV";
	T_S[9][4] = "variable";
	T_S[10][4] = "Y NV";
	T_S[19][4] = "NV";
	T_S[20][4] = "CD X OL X";
	T_S[1][5] = "R TD";
	T_S[4][5] = "C I";
	T_S[5][5] = "PE NV TD";
	T_S[6][5] = "I NV TD";
	T_S[8][5] = "; I";
	T_S[10][5] = "e";
	T_S[15][33] = "e";
	T_S[23][5] = "int";
	T_S[1][6] = "R TD";
	T_S[4][6] = "C I";
	T_S[5][6] = "PE NV TD";
	T_S[6][6] = "I NV TD";
	T_S[8][6] = "; I";
	T_S[9][6] = "e";
	T_S[23][6] = "float";
	T_S[1][7] = "R TD";
	T_S[4][7] = "I C";
	T_S[5][7] = "PE NV TD";
	T_S[6][7] = "I NV TD";
	T_S[8][7] = "; I";
	T_S[9][7] = "e";
	T_S[23][7] = "void";
	T_S[1][8] = "R TD";
	T_S[4][8] = "C I";
	T_S[5][8] = "PE NV TD";
	T_S[6][8] = "I NV TD";
	T_S[8][8] = "; I";
	T_S[9][8] = "e";
	T_S[23][8] = "char";
	T_S[1][9] = "R TD";
	T_S[4][9] = "C I";
	T_S[5][9] = "PE NV TD";
	T_S[6][9] = "I NV TD";
	T_S[8][9] = "; I";
	T_S[9][9] = "e";
	T_S[23][9] = "bool";
	T_S[9][10] = "e";
	T_S[10][10] = "Y V";
	T_S[11][10] = "valor_entero";
	T_S[19][10] = "V";
	T_S[20][10] = "V";
	T_S[9][11] = "e";
	T_S[10][11] = "Y V";
	T_S[11][11] = "valor_flotante";
	T_S[19][11] = "V";
	T_S[20][11] = "V";
	T_S[9][12] = "e";
	T_S[10][12] = "Y V";
	T_S[11][12] = "valor_caracter";
	T_S[19][12] = "V";
	T_S[20][12] = "V";
	T_S[9][13] = "e";
	T_S[10][13] = "Y V";
	T_S[11][13] = "valor_buleano";
	T_S[19][13] = "V";
	T_S[20][13] = "V";
	T_S[1][14] = "R B";
	T_S[4][14] = "C B";
	T_S[12][14] = "PA";
	T_S[13][14] = "} C { ) CD I ( for";
	T_S[1][15] = "R M";
	T_S[4][15] = "C B";
	T_S[12][15] = "M";
	T_S[14][15] = "} C { ) CD ( while";
	T_S[1][16] = "R CO";
	T_S[4][16] = "C CO";
	T_S[15][16] = "E SN S";
	T_S[16][16] = "} C { ) CD ( if";
	T_S[17][17] = "} C { ) CD ( elif";
	T_S[17][18] = "e";
	T_S[18][18] = "} C { else";
	T_S[6][19] = "e";
	T_S[9][19] = "e";
	T_S[10][19] = "Y OA";
	T_S[19][19] = "e";
	T_S[21][19] = "+";
	T_S[6][20] = "e";
	T_S[9][20] = "e";
	T_S[10][20] = "Y OA";
	T_S[19][20] = "e";
	T_S[21][20] = "-";
	T_S[6][21] = "e";
	T_S[9][21] = "e";
	T_S[10][21] = "Y OA";
	T_S[19][21] = "e";
	T_S[21][21] = "*";
	T_S[6][22] = "e";
	T_S[9][22] = "e";
	T_S[10][22] = "Y OA";
	T_S[19][22] = "e";
	T_S[21][22] = "/";
	T_S[6][23] = "e";
	T_S[9][23] = "e";
	T_S[10][23] = "Y OA";
	T_S[19][23] = "e";
	T_S[21][23] = "%";
	T_S[6][24] = "e";
	T_S[9][24] = "e";
	T_S[19][24] = "e";
	T_S[22][24] = "<";
	T_S[6][25] = "e";
	T_S[9][25] = "e";
	T_S[19][25] = "e";
	T_S[22][25] = ">";
	T_S[6][26] = "e";
	T_S[9][26] = "e";
	T_S[19][26] = "e";
	T_S[22][26] = "<=";
	T_S[6][27] = "e";
	T_S[9][27] = "e";
	T_S[19][27] = "e";
	T_S[22][27] = ">=";
	T_S[6][28] = "e";
	T_S[9][28] = "e";
	T_S[19][28] = "e";
	T_S[22][28] = "==";
	T_S[6][29] = "e";
	T_S[9][29] = "e";
	T_S[19][29] = "e";
	T_S[22][29] = "!=";
	T_S[6][30] = "e";
	T_S[9][30] = "e";
	T_S[19][30] = "e";
	T_S[22][30] = "&&";
	T_S[6][31] = "e";
	T_S[9][31] = "e";
	T_S[19][31] = "e";
	T_S[22][31] = "||";
	T_S[2][32] = "e";
	T_S[6][32] = "e";
	T_S[9][32] = "e";
	T_S[5][33] = "e";
	T_S[6][33] = "e";
	T_S[9][33] = "e";
	T_S[10][33] = "e";
	T_S[20][33] = "e";
	T_S[4][35] = "e";
	T_S[5][36] = "PE ,";
	T_S[6][36] = ",";
	T_S[9][36] = "NV ,";
	T_S[19][36] = ",";
	T_S[20][36] = ",";
	T_S[21][36] = ",";
	T_S[6][37] = "A";
	T_S[7][37] = "; Y :=";
	T_S[9][37] = "; Y :=";
	T_S[6][38] = ";";
	T_S[9][38] = "e";
	T_S[10][38] = "e";
	T_S[20][24] = "CD X OL";
	T_S[20][25] = "CD X OL";
	T_S[20][26] = "CD X OL";
	T_S[20][27] = "CD X OL";
	T_S[20][28] = "CD X OL";
	T_S[20][29] = "CD X OL";
	T_S[20][30] = "CD X OL";
	T_S[20][31] = "CD X OL";
	T_S[10][32] = ") Y (";
	T_S[19][32] = ") CD (";
	T_S[20][32] = "CD ) CD (";
	T_S[1][39] = "$";
}

class Nodo{
public:
	int linea;
	string lexema;
	string token;
	Nodo * siguiente;
	Nodo(){
		linea = -1;
		lexema = "";
		token = "";
		siguiente = NULL;
	}

	Nodo(int l, string le, string to){
		linea = l;
		lexema = le;
		token = to;
		siguiente = NULL;
	}

	Nodo(string le){
		linea = 0;
		lexema = le;
		token = "";
		siguiente = NULL;
	}

	void mostrar(){
		cout << "Lexema: " << lexema << endl;
		cout << "Token: " << token << endl;
		cout << "Linea: " << linea << endl;
		cout << "----------------" << endl;
	}

	~Nodo(){ }

};

class lista{
public:
	Nodo* raiz;
	lista(){
		raiz = NULL;
	}

	void insertar_terminal(){
		Nodo** p;
		for(p = & raiz; *p; p = &(*p)->siguiente);
		(*p) =  new Nodo(-1,"$","$");
	}

	void insertar_lexema(string le){
		if(raiz == NULL){
			raiz = new Nodo(le);
			return;
		}
		Nodo** p;
		for(p = & raiz; *p; p = &(*p)->siguiente);
		(*p) =  new Nodo(le);
	}

	bool buscar(Nodo** &p,string le){
		for(p = &raiz; (*p) != NULL; p = &(*p)->siguiente){
			if((*p)->lexema == le){
				return 1;
			}
		}
		return 0;
	}

	void eliminar(string le){
		Nodo **p;
		if(!buscar(p,le)){
			return;
		}
		(*p) = (*p)->siguiente;
	}

	void mostrar(){
		for(Nodo* p = raiz; p != NULL; p = p->siguiente){
			cout << "Lexema: " << p->lexema << endl;
			cout << "Token: " << p->token << endl;
			cout << "Linea: " << p->linea << endl;
			cout << "----------------" << endl;
		}
	}
	~lista(){ }

};

bool es_llaves(string letra){
	for(int i = 0; i < llaves.size(); i++){
		if(llaves[i] == letra){
			return 1;
		}
	}
	return 0;
}

bool es_numero(string letra){
	for(int i = 0; i < numero.size(); i++){
		if(numero[i] == letra){
			return 1;
		}
	}
	return 0;
}

bool es_letra(string letra){
	for(int i = 0; i < variable.size(); i++){
		if(variable[i] == letra){
			return 1;
		}
	}
	return 0;
}

bool es_caracter(string letra){
	for(int i = 0; i < op_artimeticos.size(); i++){
		if(letra == op_artimeticos[i]){
			return 1;
		}
	}

	for(int i = 0; i < op_logicos.size(); i++){
		if(letra == op_logicos[i]){
			return 1;
		}
	}

	for(int i = 0; i < comentarios.size(); i++){
		if(letra == comentarios[i]){
			return 1;
		}
	}

	for(int i = 0; i < simbolos.size(); i++){
		if(letra == simbolos[i]){
			return 1;
		}
	}
	return 0;
}

void acumulador_letra(string a, lista &linea, int &pos){
	string palabra;
	for( ; pos < a.size(); pos++){
		stringstream ss;
		ss << a.at(pos);
		string temp = ss.str();
		if( a[pos] == ' '){
			linea.insertar_lexema(palabra);
			return;
		}

		else if(es_caracter(temp)){
			linea.insertar_lexema(palabra);
			pos--;
			return;
		}

		else if(es_llaves(temp)){
			linea.insertar_lexema(palabra);
			pos--;
			return;
		}

		else{
			palabra+=a[pos];
		}
	}
}

void acumulador_numero(string a, lista &linea, int &pos){
	string palabra;
	for( ; pos < a.size(); pos++){
		stringstream ss;
		ss << a.at(pos);
		string temp = ss.str();
		if(a[pos] == ' '){
			linea.insertar_lexema(palabra);
			return;
		}

		else if(es_caracter(temp)){
			linea.insertar_lexema(palabra);
			pos--;
			return;
		}

		else if(es_llaves(temp)){
			linea.insertar_lexema(palabra);
			pos--;
			return;
		}

		else if(es_letra(temp)){
			linea.insertar_lexema(palabra);
			pos--;
			return;
		}

		else{
			palabra+=a[pos];
		}
	}
}

void acumulador_caracter(string a, lista &linea, int &pos){
	string palabra;
	for( ; pos < a.size(); pos++){
		stringstream ss;
		ss << a.at(pos);
		string temp = ss.str();
		if( a[pos] == ' '){
			linea.insertar_lexema(palabra);
			return;
		}

		else if(es_numero(temp)){
			linea.insertar_lexema(palabra);
			pos--;
			return;
		}

		else if(es_llaves(temp)){
			linea.insertar_lexema(palabra);
			pos--;
			return;
		}

		else if(es_letra(temp)){
			linea.insertar_lexema(palabra);
			pos--;
			return;
		}

		else{
			palabra+=a[pos];
		}
	}
	if(pos == a.size()){
		linea.insertar_lexema(palabra);
	}
}

void pre_procesamiento(char *a, lista &linea){
	string p = a;
	for(int i = 0; i < p.size() ; i++){
		stringstream ss;
		ss << p.at(i);
		string temp = ss.str();
		if(es_numero(temp)){
			acumulador_numero(p,linea,i);
		}
		else if(es_letra(temp)){
			acumulador_letra(p, linea,i);
		}
		else if(es_llaves(temp)){
			linea.insertar_lexema(temp);
		}
		else if(es_caracter(temp)){
			acumulador_caracter(p,linea,i);
		}
	}
}

bool nueva_linea(string letra){
	if(letra == "{"){
		return 1;
	}

	else if(letra == "}"){
		return 1;
	}

	else if(letra == ";"){
		return 1;
	}

	return 0;
}

void lexico(lista &pre){
	int p = 1;
	for(Nodo *j = pre.raiz; j != NULL; j = j->siguiente){
		for(int i = 0; i < tipo_dato.size();i++){
			if(j->lexema == tipo_dato[i]){
				j->token = tipo_dato[i];
				j->linea = p;
				break;
			}
		}

		for(int i = 0; i < bucles.size();i++){
			if(j->lexema == bucles[i]){
				j->token = bucles[i];
				j->linea = p;
				break;
			}
		}

		for(int i = 0; i < condicionales.size();i++){
			if(j->lexema == condicionales[i]){
				j->token = condicionales[i];
				j->linea = p;
				break;
			}
		}

		for(int i = 0; i < op_logicos.size();i++){
			if(j->lexema == op_logicos[i]){
				j->token = op_logicos[i];
				j->linea = p;
				break;
			}
		}

		for(int i = 0; i < op_artimeticos.size();i++){
			if(j->lexema == op_artimeticos[i]){
				j->token = op_artimeticos[i];
				j->linea = p;
				break;
			}
		}

		for(int i = 0; i < simbolos.size();i++){
			if(j->lexema == simbolos[i]){
				j->token = simbolos[i];
				j->linea = p;
				if(nueva_linea(j->lexema)){
					p++;
				}
				break;
			}
		}

		for(int i = 0; i < PR.size(); i++){
			if(j->lexema == PR[i]){
				j->token = PR[i];
				j->linea = p;
				break;
			}
		}

		for(int i = 0; i < llaves.size();i++){
			if(j->lexema == llaves[i]){
				j->token = llaves[i];
				j->linea = p;
				if(nueva_linea(j->lexema)){
					p++;
				}
				break;
			}
		}

		for(int i = 0; i < numero.size(); i++){
			stringstream ss;
			ss << j->lexema.at(0);
			string temp = ss.str();
			if(temp == numero[i]){
				if(temp == numero[10]){
					string aux = "Error en la linea ";
					aux += static_cast<std::ostringstream*>(&(std::ostringstream() << p))->str();
					aux += " "+j->lexema;
					aux += " numero no valido";
					errores.push_back(aux);
					pre.eliminar(j->lexema);
					break;
				}
				int punto = 0;

				for(int x = 0; x < j->lexema.size(); x++){
					stringstream pp;
					pp << j->lexema[x];
					string ww = pp.str();
					if(ww == numero[10]){
						punto++;
					}
					temp.clear();
				}

				if(punto > 1){
					string aux = "Error en la linea ";
					aux += static_cast<std::ostringstream*>(&(std::ostringstream() << p))->str();
					aux += " "+j->lexema;
					aux += " numero no valido";
					errores.push_back(aux);
					pre.eliminar(j->lexema);
					break;
				}

				else{
					if(punto == 1){
						j->token = "valor_flotante";
						j->linea = p;
						break;
					}

					else{
						j->token = "valor_entero";
						j->linea = p;
						break;
					}
				}
			}
		}

		if(j->token == ""){
			for(int i = 0; i < variable.size(); i++){
				stringstream ss;
				ss << j->lexema.at(0);
				string temp = ss.str();
				if(temp == "?"){
					string aux = "Error en la linea ";
					aux += static_cast<std::ostringstream*>(&(std::ostringstream() << p))->str();
					aux += j->lexema;
					aux += " caracter no valido";
					errores.push_back(aux);
					pre.eliminar(j->lexema);
					break;
				}

				if(temp == variable[i]){
					j->token = "variable";
					j->linea = p;
					break;
				}
			}
		}

		if(j->token == ""){
			string aux = "Error en la linea ";
			aux += static_cast<std::ostringstream*>(&(std::ostringstream() << p))->str();
			aux += j->lexema;
			aux += " caracter no valido";
			errores.push_back(aux);
			pre.eliminar(j->lexema);
		}
	}
}

lista leer(const string nfichero){
	lista lexema;
	char cadena[80];
	ifstream fichero;
	fichero.open(nfichero.c_str());
	if(!fichero.fail()){
		fichero.getline(cadena,80, '\n');
		while(!fichero.eof()){
			pre_procesamiento(cadena,lexema);
			fichero.getline(cadena, 80, '\n');
		}
		fichero.close();
	}
	return lexema;
}

int buscar_token(string t){
	for(int i = 0; i < 40; i++){
		if(T_S[0][i] == t){
			return i;
		}
	}
	return -1;
}

int buscar_P(string nt){
	for(int i = 0; i < 25; i++){
		if(T_S[i][0] == nt){
			return i;
		}
	}
	return -1;
}

bool existe(int x, int y, vector<string> &raiz){
	string p = T_S[x][y];
	if(p == " "){
		return 0;
	}
	string aux;
	raiz.pop_back();
	for(int i = 0; i < p.size(); i++){
		if(p[i] == ' '){
			raiz.push_back(aux);
			aux = "";
		}
		else{
			aux+=p[i];
		}
	}
	raiz.push_back(aux);
	return 1;
}

void mostrar_vector(vector<string> a){
	for(int i = 0; i < a.size(); i++){
		cout << a[i] << "+";
	}
	cout << endl;
}

string tam(string td){
	if(td == "int"){
		return "4";
	}

	else if(td == "float"){
		return "8";
	}

	else if(td == "char"){
		return "1";
	}

	else if(td == "null"){
		return "0";
	}

	else if(td == "bool"){
		return "1";
	}
}

bool v_existe(string a){
	for(int i = 0; i <  T_SE.size(); i+=6){
		if(a == T_SE[i]){
			return 1;
		}
	}
	return 0;
}

bool sintactico(lista l){
	vector<string> r;
	int err = 0;
	string tipo_variable;
	string funcion = "No";
	string ParaEntra;
	r.push_back("$");
	r.push_back("R");
	Nodo *p = l.raiz;
	while(p->siguiente){
		mostrar_vector(r);
		p->mostrar();
		int T = buscar_token(p->token);
		cout << "token: " << T << endl;
		int P = buscar_P(r[r.size()-1]);
		cout << "P: " << P << endl;
		bool v;
		if (T > 0 && P > 0 )
		{
			v = existe(P,T,r);
		}

		else{
			if(P < 0){
				string q = "Error en la linea ";
				q += static_cast<std::ostringstream*>(&(std::ostringstream() << p->linea))->str();
				q += " caracter no esperado " + p->token;
				errores.push_back(q);
				r.pop_back();
			}

		}

		if(r[r.size()-1] == "TD"){
			tipo_variable = p->token;
		}

		if(r[r.size()-1] == "fun"){
			funcion = "Si";
		}

		cout << "Existe: " << v << endl;
		cout << "-------------------" << endl;
		if(v == 0){
			string q = "Error en la linea ";
			q += static_cast<std::ostringstream*>(&(std::ostringstream() << p->linea))->str();
			q += " caracter no esperado " + p->token;
			errores.push_back(q);
			int NL = p->linea+1;
			err++;
			while(p->linea < NL && p->siguiente){
				p = p->siguiente;
			}
		}

		else{
			while(r[r.size()-1] == p->token && p->siguiente){
				if(r[r.size()- 1] == "variable"  && funcion == "No" && tipo_variable != ""){
					T_SE.push_back(p->lexema);
					T_SE.push_back(tipo_variable);
					if(p->siguiente->lexema == "=:=" ){
						T_SE.push_back(p->siguiente->siguiente->lexema);
					}

					else{
						T_SE.push_back("NULL");
					}

					T_SE.push_back(tam(tipo_variable));
					T_SE.push_back(funcion);
					T_SE.push_back("--------");
					while(p->siguiente->lexema == ","){
						T_SE.push_back(p->siguiente->siguiente->lexema);
						T_SE.push_back(tipo_variable);
						T_SE.push_back("NULL");
						T_SE.push_back(tam(tipo_variable));
						T_SE.push_back(funcion);
						T_SE.push_back("--------");
						p = p->siguiente->siguiente;
					}

					tipo_variable.clear();
				}

				else if((r[r.size()- 1] == "variable" || r[r.size()- 1] == "principal")  && funcion == "Si" && tipo_variable != ""){
					T_SE.push_back(p->lexema);
					T_SE.push_back(tipo_variable);
					T_SE.push_back("NULL");
					T_SE.push_back(tam(tipo_variable));
					T_SE.push_back(funcion);
					string pe = "";
					for(Nodo *h = p; h->linea == p ->linea; h = h->siguiente){
						if(h->token == "variable"){
							pe+=h->lexema;
							pe+=",";
						}
					}
					T_SE.push_back(pe);
					funcion = "No";
					tipo_variable.clear();
				}

				else{
					if((r[r.size()-1] == "variable") && tipo_variable == ""){
						Variable_S.push_back(p->lexema);
					}
				}
				r.pop_back();
				p = p->siguiente;
			}
			if(r[r.size()-1] == "e"){
				r.pop_back();
				while(r[r.size()-1] == p->token && p->siguiente){
					mostrar_vector(r);
					cout << p->token << endl;
					r.pop_back();
					p = p->siguiente;
				}
			}
		}
	}

	if(err == 0){
		return 1;
	}
	return 0;
}

void levantar_datos(){
	llenar_datos();
	llenar_TS();
}

void errores_txt(){
	ofstream ar("tabla_errores.txt");
 	for(int i = 0; i < errores.size(); i++){
		ar << errores[i] << endl;
	}
	ar.close();
}

bool llenar_T_SE(vector<string> &b){
	bool r;
	for(int i = 0; i < Variable_S.size(); i++){
		if(!v_existe(Variable_S[i])){
			T_SE.push_back(Variable_S[i]);
			T_SE.push_back("------------");
			T_SE.push_back("------------");
			T_SE.push_back("------------");
			T_SE.push_back("------------");
			T_SE.push_back("------------");
			r = 1;
			b.push_back(Variable_S[i]);
		}
	}
	return r;
}

void sintactico_txt(){
	vector<string> b;
	if(llenar_T_SE(b)){
		for(int i =0 ; i < b.size(); i++){
			string ss = "Variable no instanseada ";
			ss += b[i];
			errores.push_back(ss);
		}
	}

	ofstream TSE("tabla_sintactica.txt");
	TSE << "Tabla Sintactica" << endl;
	TSE << "N_Variable " << '\t' ;
	TSE << "Tipo de Dato " << '\t';
	TSE << "Valor Guardado" << '\t';
	TSE << "Tamaño de Dato " << '\t';
	TSE << "Funcion " << '\t';
	TSE << "Variable de Entrada" << '\t';
	for(int i = 0; i < T_SE.size(); i++){
		if(i%6 == 0){
			TSE << endl;
		}
		TSE << T_SE[i] << '\t' << '\t';
	}

	TSE.close();
}

void mostrar_sintactico(lista l){
	l.insertar_terminal();
	if(sintactico(l) == 1){
		cout << "Codigo ..... OK!" << endl;
	}

	else{
		cout << "Codigo ..... ERROR!" << endl;

	}
}

int main(){
	levantar_datos();
	lista lex;
	lex = leer("codigo1.txt");
	lexico(lex);
	//lex.mostrar();
	mostrar_sintactico(lex);
	sintactico_txt();
	errores_txt();
	return 0;
}
