#ifndef LEXICO1_H_INCLUDED
#define LEXICO1_H_INCLUDED

#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include "List.h"
using namespace std;

/*void print_lista(vector<string> &vectorEntrada){

    for(int i = 0; i < vectorEntrada.size(); i++){
        cout<< "[" << vectorEntrada[i] << "]" << endl;
    }
}

void print_matriz(vector<vector<string> > &matriz){

    for(int i = 0; i < matriz.size(); i++){
        for(int j = 0; j < matriz[i].size(); j++){
            cout << matriz[i][j] << "  ";
        }
        cout << endl;
    }
}*/



class Lexico1{

    public:
      List salida;
      List errores;
      string cadena;
      int i;
      string token;
      vector<string> separados;
      vector<string> comentarios;
      vector<vector<string> > matriz;
      bool flagFinLinea = false;


    Lexico1(){
        cadena = "";
        token = "";
        i = 1;
    }
    bool isNumero(string cadena){
        int i = 0;

        while( i< cadena.size() ){
        if(cadena[i] >= 48 &&  cadena[i] <= 57){
            i++;
        }else{
            return 0; }
        }
        return 1;
    }

    int isFloat(string cadena){
        string aux,aux2; int j=-1;
        if(cadena[0] == '.')
          return 0;
        for (int i = 0 ; i< cadena.size(); i++){
          j=cadena.find('.');
          aux = cadena.substr (0,j);
          aux2 = cadena.substr (j+1,cadena.size()-(j+1));
          if(isNumero(aux) && isNumero(aux2))
              return 1;

          else {
            return 0;
          }

        }

    }

    int	isCadena(string cadena){
        int i = 0 ;
        int flag=0;
        while (i< cadena.size() ) {
            if(cadena[i] >=65 &&  cadena[i]<= 90 || cadena[i]>=97 && cadena [i]<=122 || cadena[i] >= 48 &&  cadena[i] <= 57
              || cadena[i] == 95)
            i++;
            else {
                return 0;
            }
        } return 1;
    }

    vector<string> split2(string str, char delimiter){

        vector<string> internal;
        stringstream ss(str);
        string tok;
        string tok2;
        string sep = ";";
        int pos = 0;
        if (str.empty())
        {
          return internal;
        }
        else{
          while(getline(ss, tok, delimiter)) {
            stringstream s1(tok);
            pos = tok.find(';');
            if (pos > 0)
            {
              getline(s1,tok2,';');
              internal.push_back(tok2);
              internal.push_back(sep);
            }
            else
            {
              internal.push_back(tok);
            }
          }

        }

        return internal;
    }

    vector<int> minimo(string sep, string aux)
    {
      vector<int> resultado;
      int j = -1,pos=0;
      int min = 1000;
      for(int i = 0;i<sep.size();i++)
      {
        pos = aux.find(sep[i]);
        if(min>pos && pos>-1)
        {
          min = pos;
          j = i;
        }

      }
      resultado.push_back(j);
      resultado.push_back(min);
      return resultado;
    }


    vector<string> split(string str, char delimiter){

        vector<string> internal;
        vector<int>resultado;
        stringstream ss(str);
        string tok;
        string tok2;
        string sepa;
        string aux;
        string sep = "=()[]{}><+-,;*/!&|!";
        int pos = 0;
        while(getline(ss, tok, delimiter)) {
            stringstream s1(tok);
            aux = tok;
            while(aux.length()!=0)
            {
              resultado=minimo(sep,aux);
              if (aux[0] == 34)
              {
                string aux2 = aux.substr(0,aux.size()-1);
                internal.push_back(aux2);
                aux = aux.substr (aux.size()-1,aux.size());
                s1.str(aux);
              }
              else if(resultado[1]!=1000 && resultado[0]!=-1)
              {
                sepa = sep[resultado[0]];
                getline(s1,tok2,sep[resultado[0]]);
                aux = aux.substr (resultado[1]+1,aux.size());

                if(tok2.length()==0 && aux !=sepa)
                {
                  internal.push_back(sepa);
                }
                else if(aux == sepa)
                {
                  if(tok2.length()>0)
                  {
                    internal.push_back(tok2);
                  }
                  internal.push_back(sepa);
                  aux = "";
                }
                else
                {
                  internal.push_back(tok2);
                  internal.push_back(sepa);
                }
              }
              else
              {
                if(aux.size()>=1)
                {
                  internal.push_back(aux);
                }
                else
                {
                  internal.push_back(tok);
                }
                aux = "";
                pos=-1;

              }
            }

        }
        return internal;
    }


    void procesar(){
        ifstream codigoEntrada("prueba.txt");
        string lineaCodigo = " ";

        if(codigoEntrada.is_open()){
            while(getline(codigoEntrada, lineaCodigo)){
                if (lineaCodigo[0] == '#'){
                    comentarios.push_back(lineaCodigo);
                    continue;
                }
                else{
                    transform(lineaCodigo.begin(), lineaCodigo.end(), lineaCodigo.begin(), ::tolower);
                    separados = split(lineaCodigo,' ');
                    matriz.push_back(separados);
                }
            }
            codigoEntrada.close();
        }else{
            cout << "No se pudo abrir el archivo";
        }

        ofstream archivoTokens;
        archivoTokens.open("listaTokens.txt");

        for(int i = 0; i < matriz.size(); i++){
            archivoTokens<<"-------------INICIO LINEA "<< i+1 << "-------------"<<endl;
            for(int j = 0; j < matriz[i].size(); j++){
                string tmp = matriz[i][j];
                string tmp1;
              if (tmp == "int" || tmp == "float" || tmp == "string" || tmp == "char" || tmp == "void"){
                salida.add(i,"TIP_DAT",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : TIP_DAT \t";
                archivoTokens<<  ",TOKEN: "<<tmp <<endl;
              }
              else if (tmp == "*" || tmp == "-" || tmp == "+" || tmp == "/" || tmp == "%"){
                tmp1 = matriz[i][j+1];
                if(tmp1 == "+" || tmp1 == "-")
                {
                  salida.add(i,"OPE_INCRE",tmp);
                  archivoTokens << i<< "\t";
                  archivoTokens<< "LEXEMA : OPE_INCRE \t";
                  archivoTokens<<  ",TOKEN: "<< tmp+tmp1 <<endl;
                  j++;
                }
                else
                {
                  salida.add(i,"OPE_ARIT",tmp);
                  archivoTokens << i<< "\t";
                  archivoTokens<< "LEXEMA : OPE_ARIT \t";
                  archivoTokens<<  ",TOKEN: "<< tmp <<endl;
                }

              }
              else if (tmp == "<" || tmp == ">" || tmp == "!"){
                tmp1 = matriz[i][j+1];
                  if (tmp1 == "="){
                  salida.add(i,"OPE_CMP",tmp);
                  archivoTokens << i<< "\t";
                  archivoTokens<< "LEXEMA : OPE_CMP \t";
                  archivoTokens<<  ",TOKEN: "<< tmp+tmp1 <<endl;
                  j++;
                }
                else
                {
                  salida.add(i,"OPE_CMP",tmp);
                  archivoTokens << i<< "\t";
                  archivoTokens<< "LEXEMA : OPE_CMP \t";
                  archivoTokens<<  ",TOKEN: "<< tmp <<endl;
                }

              }

              else if (tmp == "and" || tmp == "or"){
                salida.add(i,"OPE_LOG",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : OPE_LOG \t" ;
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "main"){
                salida.add(i,"MAIN",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : MAIN \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "return"){
                salida.add(i,"RETURN",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : RETURN \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "break"){
                salida.add(i,"BREAK",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : BREAK \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "#"){
                salida.add(i,"MACRO",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : MACRO \t" ;
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp[0] == 34 && tmp[tmp.size() -1] == 34){
                salida.add(i,"STRING",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : STRING \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp[0] == 39 && tmp[tmp.size() -1] == 39){
                salida.add(i,"CHAR",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : CHAR \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "("){
                salida.add(i,"PAR_OP",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : PAR_OP \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp[0]==')'){
                salida.add(i,"PAR_CL",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : PAR_CL \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp[0]=='{'){
                salida.add(i,"LL_OP",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : LL_OP \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp[0]=='}'){
                salida.add(i,"LL_CL",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : LL_CL \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "["){
                salida.add(i,"COR_OP",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : COR_OP \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "]"){
                salida.add(i,"COR_CL",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : COR_CL \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == ","){
                salida.add(i,"COMA",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : COMA \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == ";"){
                salida.add(i,"FINL",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : FINL \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
                flagFinLinea=1;
              }
              else if (tmp == "="){
                tmp1 = matriz[i][j+1];
                  if (tmp1 == "="){
                  salida.add(i,"OPE_CMP",tmp);
                  archivoTokens << i<< "\t";
                  archivoTokens<< "LEXEMA : OPE_CMP \t";
                  archivoTokens<<  ",TOKEN: "<< tmp+tmp1 <<endl;
                  j++;
                }
                else
                {
                  salida.add(i,"IGUAL",tmp);
                  archivoTokens << i<< "\t";
                  archivoTokens<< "LEXEMA : IGUAL \t";
                  archivoTokens<<  ",TOKEN: "<< tmp <<endl;
                }

              }
              else if (tmp == "if"){
                salida.add(i,"IF",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : IF \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "else"){
                salida.add(i,"ELSE",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : ELSE \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "elif"){
                salida.add(i,"ELSEIF",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : ELSEIF \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "while"){
                salida.add(i,"WHILE",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : WHILE \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "for"){
                salida.add(i,"FOR",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : FOR \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == " " || tmp == "\t" || tmp == "\n" || tmp == ""){
                j++;
              }
              else if (isNumero(tmp)){
                salida.add(i,"NUMERO_ENTERO",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : NUMERO_ENTERO \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (isFloat(tmp)){
                salida.add(i,"NUMERO_FlOTANTE",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : NUMERO_FlOTANTE \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (isCadena(tmp)){
                salida.add(i,"NOM_VAR",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : NOM_VAR \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else {
                salida.add(i,"ERROR",tmp);
                archivoTokens << i<< "\t";
                archivoTokens<< "LEXEMA : ERROR \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
            }
        }
        archivoTokens.close();
    }
};

#endif // LEXICO_H_INCLUDED
