#ifndef SINTACTICO_H
#define SINTACTICO_H
#include "Lexico1.h"
#include <vector>
#include <stack>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include "Nodo.h"

using namespace std;


class Sintactico
{
    public:
        Sintactico(char *nom);
        bool buscar();
        bool terminal(string n);
        void agregar_pila(string n);
        string acceder(string n , string m);
        bool analizar();
        virtual ~Sintactico();
        void print_errores();
        stack<string> errores;
    protected:
        string tabla[25][30];
        Lexico1 lex;
        stack<string> pila;
};

#endif // SINTACTICO_H
