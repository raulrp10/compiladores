/*
...::: by Alejandro Martinez Camacho :::...
Instituto Tecnologico de Zacatecas
 2007

Analizador lexico y sintactico para el lenguaje pl0
Ejemplo:

const Max=100,alex=300;
var i,cont,j,k;
inicio
	para(i=1 & Max*2)
	inicio
		leer cont;
		para(j=Max % 1)
			mientras(i<=cont + Max/3)
				para(k=j+cont & j*Max)
					cont=i+j*k-2;
		escribir cont
	fin
fin.
*/

//#include <iostream.h>
//#include <conio>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctype.h>
using namespace std;

//definicion de TRUE o FALSE con valores de 1 y 0
#define TRUE 1
#define FALSE 0
//tama寸 del arreglo para almacenar todas las lineas
#define MAX 100

/*Declaracion de procedimientos usados por el analizador sintactico
 */
void leetoken();
void errores(int);
void Programa();
void Bloque();
void Factor();
void Termino();
void Expre();
void aux_e();
void aux_t();
void mm();
void md();
void operadoresLogicos();
void Condicion();
void Inicio();
void inicio2();
void Propocisiones();
void aux_inicio();
void Declaracion();
void Escribir();
void Si();
void Para();
void Mientras();
void Leer();
void Llamar();
void Constante();
void Variable();
void var_aux();
void const_aux();
void Declaraciones1();
void Declaraciones2();
void DeclararConstante();
void DeclararVariable();
void const_aux();
void var_aux();
void Procedimiento();

int numero,errorGlobal=0,pocision,flag=0;
int errorLexico=FALSE, errorSintactico=FALSE;
char nomArchivo[255];
char nombretok[50];
char lineacompleta[80][MAX]; //arreglo para almacenar las lineas

class Lexico
{
  public:
	int errorLectura,comentarioMul, comentarioSim;
	Lexico()
	{
		lex=NULL; //Nodo de la lista de tokens
		errorLectura=0;
		flag=0;
		strcpy(tabla[0],"");		//Segun la tabla de simbolos dada
		strcpy(tabla[1],"CONST");       //cada uno de los tokens de esta
		strcpy(tabla[2],"VAR");         //relacionado con su indice dentro del
		strcpy(tabla[3],"PROC");        //arreglo para asi poder saber su numero
		strcpy(tabla[4],"INICIO");
		strcpy(tabla[5],"FIN");
		strcpy(tabla[6],"ESCRIBIR");
		strcpy(tabla[7],"LEER");
		strcpy(tabla[8],"LLAMAR");
		strcpy(tabla[9],"SI");
		strcpy(tabla[10],"MIENTRAS");
		strcpy(tabla[11],"PARA");
		strcpy(tabla[12],"ID");
		strcpy(tabla[13],"NUM");
		strcpy(tabla[14],".");
		strcpy(tabla[15],"=");
		strcpy(tabla[16],",");
		strcpy(tabla[17],";");
		strcpy(tabla[18],"(");
		strcpy(tabla[19],")");
		strcpy(tabla[20],"&");
		strcpy(tabla[21],"%");
		strcpy(tabla[22],"==");
		strcpy(tabla[23],"#");
		strcpy(tabla[24],"<");
		strcpy(tabla[25],">");
		strcpy(tabla[26],"<=");
		strcpy(tabla[27],">=");
		strcpy(tabla[28],"+");
		strcpy(tabla[29],"-");
		strcpy(tabla[30],"*");
		strcpy(tabla[31],"/");
	}

	struct Lexemas //Lista enlazada que almacena los lexemas
	{
		char nombre[32];  //Almacena los datos del lexema
		int numero,linea; // numero simbolo y linea
		Lexemas *izq,*der;//2 punteros para recorrido de inicio a fin y viceversa
	}*lex; //apuntador a la lista

	int token;       //Variables globales para el numero segun la tabla
	char nombre[20]; //de simbolos y su nombre

	//Funcion que agrega a la lista enlazada de tokens uno nuevo
	//recibiendo como paramentro la linea en la que se encuentra
	//su numero de la tabla de simbolos y el lexema formado
	void add(int linea,int numero, char *nombre)
	{
		Lexemas *cur; //apuntador local que almacena los datos de los parametros
		cur = new (Lexemas); //creamos el nuevo nodo
		strcpy(cur->nombre,nombre); //Asignamos a los campos de la lista
		cur->numero=numero;         //su respectivo valor
		cur->linea=linea;
		cur->izq=lex;	//Hacemos que este nodo por la izquierda apunte al nodo lex
		lex->der=cur;   //Hacemos que el nodo lex por la derecha apunte al nodo actual
		cur->der=NULL;  //el apuntador actual por la derecha no contiene nada
		lex=cur;
		if(flag==0) //Si fue la primer nodo agregado a la lista establecemos que el
		{first=lex; flag++;} //nodo first es igual a lex que fue el primero
	}

	int obtenToken() //Obtiene el numero del siguiente elemento de la lista
	{
		int val;
		if(flag==1) //si ya fue inicializado el primer nodo
		{x=first;flag--;} //se asigna al puntero X el primer nodo (first)
		if(x==NULL) //Si fue nulo ya no hay tokens que leer
			return -1;
		val=x->numero;
		token=val; 		  //Obtenemos los valores del nodo
		pocision=x->linea;        //y los asigamos a val, token, pocision y nombre
		strcpy(nombre,x->nombre);
		strcpy(nombretok,nombre);
		x=x->der; //apuntamos el siguiente nodo de la lista para la proxima vez que llamemos al metodo
		return val;  //regresa su valor segun sea en la tabla de simobolos
	}

	//Obtiene solo el nombre del archivo .pl0 a partir de la ruta completa
	//pasada como argumentos, de los dados cuando ejecutamos el programa
	char * obtenNombre(char args[])
	{
		int x;
		char l='\x0', nombre[35],aux[10];
    int i = 0;
		strcpy(nombre,NULL);
		for(int i=strlen(args); i>=0; i--){ //recorremos la ruta hasta encontrar una '\'
			l=args[i];
			if(l=='\\')  //Si ya la encontramos salimos del ciclo
				break;
		}
		//pasamos todos los caracteres a partir de donde se encontro '\'
		//a la cadena nombre
		for(int j=i+1; j<strlen(args); j++,x++){
			sprintf(aux,"%c",args[j]);
			strcat(nombre,aux);
		}
		return nombre; //regresamos dicha cadena que contiene solo el nombre
	}

	//Metodo que regresa verdadero o falso ( 1 o 0 ) si la letra dada
	//corresponde o no a los caracteres validos del alfabeto
	int esInvalido(char let)
	{
	      if(let!='\n' && let !='\t' && let!='\r' && let!='.' &&
		 let!=',' && let!=';' && let!=' ' && let!='(' && let!=')' &&
		 let!='+' && let!='-' && let!='*' && let!='/' && let!='\x0' &&
		 let!='<' && let!='>' && let!='=' && let!='&' && let!='%')
		 return 1;
	      else
		return 0;

	}

	//Metodo que recorre la lista enlazada de tokens y los muestra a pantalla
	void muestraTokens(char *archivo)
	{
		int num;
		cout<<"\n#TOKEN\t\tLEXEMA"<<endl;
		do
		{
			num = obtenToken();
			strcpy(nombre,strlwr(nombre));
			if(num==-1) //Cuando es -1 no hay mas tokens
				break;
			if(num==-100)//Cuando es -100 error de ID
			{
				printf("%s : Error en linea %d : Identificador No Valido\n\t>>  %s",obtenNombre(archivo),pocision,nombre);
				break;
			}
			if(num==-101)//Cuando es -101 es error de numero
			{
				printf("%s: Error en linea %d : Numero Entero Invalido\n\t>>  %s",obtenNombre(archivo),pocision,nombre);
				break;
			}
			if(num==-102)//Cuando es -102 es error de caracter invalido
			{
				printf("%s: Error en linea %d : Elemento No Valido\n\t>> %s",obtenNombre(archivo),pocision,nombre);
				break;
			}
			if(num!=12 & num!=13) //si no es un ID o NUM asignamos nulo al lexema
				strcpy(nombre,"");
			cout<<num<<"\t\t"<<nombre<<endl; //Mostramos la informacion resultante del token
		}while(num!=-1);
		getch();
	}

	//Metodo que abre el archivo para su lectura y se encarga de formar
	//los lexemas y clasificarlos
	void leeDatos(char *archivo)
	{
		char let;
		char lexema[20];
		int letra,contador,palabras;
		char linea[80];
		fstream capturaDatos(archivo,ios::in); //abrimos el archivo para lectura
		if(!capturaDatos)
		{
			cout<<"Archivo inexistente / Error de lectura";
			getch();        //Si hubo un error de lectura o apertura
			errorLectura=1; //salimos del metodo y asignamos 1 a la variable
			return;         //errorLectura
		}
  contador=0; //contador de lineas, inicialmente 0
  int p=0,k,i=0,z; //variables para los ciclos e indices de los arreglos de cadenas
  int numero=-1; //numero del lexema segun la tabla, inicialmente -1
  comentarioMul=0; //bandera para saber si hubo comentarios de multiples lineas, establecido a 0
  char *palabra;

  while(!capturaDatos.eof()) //mientras no sea fin de archivo
  {
	   capturaDatos.getline(linea,80,'\n');  //obtenemos una linea del archivo
	   strcpy(linea,strupr(linea)); //se convierte a minusculas toda la linea
	   strcpy(lineacompleta[contador+1],linea);
	   p=0;
	   let='\0';        //el valor de let es NULO
	   comentarioSim=0; //Inicializacion a cero para los comentarios simples de una linea
	   int errorID=0;

	 for(k=0; k<=20; k++)
	 {lexema[k]='\0';} //para dejar en blanco de nuevo la cadena

	  for(i=0; i<strlen(linea); i++) //recorremos toda la linea de 0 a n
	  {
		let=linea[i];

		if(let=='/' && linea[i+1]=='/') //Si hay 2 / / juntas ponemos la bandera a 1
			comentarioSim=1;        //esto para ignorar los caracteres hasta fin de linea
		if(comentarioSim==1 && let=='\n') //Si ya fue fin de linea regresa a tu estado normal
			comentarioSim=0;
		if(let=='}') //si es un  '}' regresan a su estado normal los comentarios multiples
			comentarioMul=0;
		if(comentarioMul==1 || comentarioSim==1)
			continue;   //En cualquier caso de que las banderas esten activas se ignora el caracter leido

		//En caso de ser espacio, nueva linea, tabulador o retorno de carro lo ignora
		if (let==' ' | (int)let=='\n' | (int)let=='\t' | (int)let=='\r')
		{continue;}
		if (isalpha((int)let)) //En caso de ser una letra [A-Z][a-z]
		{
		 //mientras siga siendo letra, numero o subrayado ....
		 while(isalpha((int)let) || isdigit((int)let) || let=='_')
		 {
			lexema[p]=let; //agregamos la letra al lexema
			i++;
			let=linea[i]; //asignamos la proxima letra
			p++;
			if(isalpha((int)let) | isdigit((int)let) | let=='_')
				continue;
			if(esInvalido(let)) //Si es un caracter que no este dentro de los definidos
			{
				int flag=0;
				while(esInvalido(let)) //se repite hasta que lea todo el token malformado
				{
					lexema[p]=let; //agregamos la letra al lexema
					i++;
					let=linea[i]; //asignamos la proxima letra
					p++;
					flag++;
				}
				if(flag>0)
				{
					errorID=1;
					errorLexico=TRUE;
					add(contador+1, -100,lexema);
				}
			}
			if(errorID==1) //Si hubo un error nos salimos
				break;
		 }
		 numero=-1;
		 for(z=1; z<=11; z++)
		 {
			if(strcmp(lexema,tabla[z])==0) //compara si el lexema formado
			{                             //esta en la tabla de simbolos
				numero=z; //se asigna la pocision o numero del simbolo
				add(contador+1,z,lexema); //agrega un nuevo nodo a la lista
				break;
			}
			if(z==11)//En caso de haber llegado al fin y no haber identificado la palabra reservdada
			{        //lo asigna automaticamente como identificador
				add(contador+1,12,lexema);
			}
		 }
		 for(k=0; k<=20; k++)
		 {lexema[k]='\0';} //limpiamos el arreglo
		 p=0;
		 i=i-1; //regresamos un caracter en la linea
	    }
	 else  if (isdigit((int)let)) //En caso de ser numero
	 {
		while(isdigit((int)let)) //mientras siga siendo numero
		{
			lexema[p]=let; //se va guardando en el arreglo
			i++;
			let=linea[i]; //obtenemos la siguente letra
			p++;
			if(isdigit((int)let))
				continue;
			if(esInvalido(let)) //verificamos que no sea invalida la letra
			{
				int flag=0;
				while(esInvalido(let))
				{
					lexema[p]=let; //agregamos la letra al lexema
					i++;
					let=linea[i]; //asignamos la proxima letra
					p++;
					flag++;
				}
				if(flag>0)
				{
					errorID=1;
					errorLexico=TRUE;
					add(contador+1, -101,lexema);

				}
			}
			if(errorID==1)
				break;
		}
		//ya que no sigue siendo numero, se agrega a la lista
		add(contador+1,13,lexema);
		for(k=0; k<=20; k++)
		{lexema[k]='\0';} //limpiamos el arreglo
		p=0; //apunta al indice del arreglo lexema al comienzo
		i=i-1; //recorremos una letra en la linea
	 }
	 else //si no fue ninguno de los casos anteriores.....
	 {
		lexema[p]=let;
		if(let=='=' | let=='<' | let=='>')
		{
			char aux=linea[i+1]; //obtenemos la proxima letra
			if(aux=='=') //en caso se ser un simbolo como == <= o >=
			{
				lexema[p+1]=aux; //le agrega un = a la letra leida anteriormente
				i++;
			}
		}
		numero=0;
		int flag=0;
		for(z=14; z<=31; z++) //busca dentro de la tabla para asignar su numero correspondiente
		 {

			if(strcmp(lexema,tabla[z])==0)
			{
				numero=z;
				flag=1; //establece la bandera a 1 ya que lo encontro
				add(contador+1,numero,lexema);
				break;
			}
		 }
		 int flag2=0;
		 if(let=='{') //se verifica si la letra es  '{'
		 {
			comentarioMul=1;
			flag2=1;
		 }
		 else if(let=='}') // caso contrario a lo anterior
		 {
			comentarioMul=0;
			flag2=1;
		 }
		 if(flag==0 && flag2!=1) //Si se llega hasta este punto es porque el caracter no entro
		 {                       //dentro de ninguna clasificacion por lo que es un elemento invalido
		     flag2=0;
		     char cadena[10];
		     sprintf(cadena,"%c",let);
		     errorLexico=TRUE;
		     add(contador+1,-102,cadena);
		     break;
		 }

		 for(k=0; k<=20; k++)
		 {lexema[k]='\0';} //limpiamos el arreglo
		p=0;

	     }
	   }
	   contador++;  //incrementamos el numero de lineas
	 }
	capturaDatos.close();
	}
	private:
		int flag;
		char tabla[31][10];
		int indice;
		char lexemas[100][20];
		Lexemas *first,*x;
};

Lexico objLex; //Objeto de la clase Lexico

///////////////////////////////////////////////////
// INICIO DE PROCEDIMIENTOS DE ANALIZADOR SINTACTICO
///////////////////////////////////////////////////
/*Muestra un mensaje de error de acuerdo a su numero
  nombrearchivo: error en linea N : se esperaba X
     linea de codigo
 **/

void errores(int numero)
{
	switch(numero)
	{
		case 10: cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  ."; break;
		case 30: cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  ( ";break;
		case 5:  cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  Operador Logico < > <= >= = =="; break;
		case 20: cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  ; "; break;
		case 18: cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  , "; break;
		case 22: cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  Sentencia o Propocision "; break;
		case 17: cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  Numero "; break;
		case 8:  cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  Identificador "; break;
		case 24: cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  Numero, Identificador o ( "; break;
		case 12: cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  = "; break;
		case 7:  cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  Numero o Identificador "; break;
		case 11: cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  ) "; break;
		case 16: cout<<nomArchivo<<": Error en linea "<<pocision<<" : "<<"Se esperaba  & o % "; break;
	}
	cout<<endl<<"   "<<lineacompleta[pocision]<<endl;
	errorGlobal=1; //para saber si hubo o no ningun error global sintactico
	getch();
}
/*Manda llamar a la funcion obtenToken de la clase Lexico para
  obtener el numero correspondiente a cada token
 */
void leetoken()
{
	numero = objLex.obtenToken();
	if(numero==-1)
		return;
}
/*Regla Principal de la gramatica donde empieza el analisis sintactico y semantico
 **/
void Programa()
{
	flag=TRUE;//bandera inicialmente en falso para errores en sentencias anidadas
	Bloque(); //se manda llamar a la Fucion Bloque
	if(errorGlobal==FALSE)
		if(numero!=14)
			errores(10);
		else{ //Si no hubo ningun error entonces esta bien
			if(errorGlobal!=1)
				cout<<endl<<"Programa Correcto!\n\n\nPresione cualquier tecla para continuar...";
				getch();
		}
}

/*Funcion de Bloque que tiene a las 4 reglas de la gramatica
  de CONST,VAR, PROC y cualquier PROPOCISION O SENTENCIA
 */
void Bloque()
{
	DeclararConstante();
	DeclararVariable();
	Procedimiento();
	Propocisiones();
}
/*Funcion en la que se hacen las declaraciones de constantes
 */
void DeclararConstante()
{
	if(numero!=1) //si no es el token CONST se sale de la funcion
		return;
	leetoken(); //lee el proximo token
	Constante(); //llama a la funcion Constante()
	if(numero!=17)
		errores(20); //Si cuando termina de declarar no hay un ;
	else                 //hay error
		leetoken();  //sino leemos el siguiente token
}
/*Funcion encargada de verificar la sintaxis de una declaracion
  por ejemplo var=10
 */
void Constante()
{
	if(numero==12) //Verifica si es un identificador
	{
		leetoken();
		if(numero!=15) //verifica  si es un =
			errores(12);
		else
		{
			leetoken();
			if(numero!=13) //verifica si es un numero
				errores(17);
			else
			{
				leetoken();
				const_aux();//llama a esta funcion
			}
		}
	}
	else
		errores(8);
}

/*Funcion para saber s hay mas declaraciones de constantes
  separadas por ,
 */
void const_aux()
{
	if(numero!=17)//verifica si es ; si lo es sale de la declaracion
	{
		if(numero!=16) //verifica si es una ,
			errores(18);
		else //si fue , lee otro token y llama a la siguiente declaracion
		{
			leetoken();
			Constante();
		}
	}
}
/*Funcion para la declaracion de variables
 */
void DeclararVariable()
{
	if(numero!=2)  //si no es el token VAR sale de la funcion
		return;
	leetoken();
	Variable(); //llama a la funcion Variable
	if(numero!=17)   //verifica que saliendo de variable haya un ;
		errores(20); //si no es ; hay error
	leetoken();
}

/*Funcion que hace el analisis de una sola declaracion de una variable

 */
void Variable()
{
	if(numero!=12)//verifica si es un identificador
		errores(8);
	else
	{
		leetoken();
		var_aux(); //llama a esta funcion para saber si hay mas declaraciones
	}
}

/*Funcion encargada de verificar si hay mas declaraciones de variables
   delimitadas por la ,
 */
void var_aux()
{
	if(numero!=17)//Si es un ; sale de la funcion
	{
		if(numero!=16) //Verifica si hay una ,
			errores(18);
		else
		{
			leetoken(); //En caso de ser , lee el proximo token
			Variable(); //y llama a esta funcion para hacer otra declaracion
		}
	}
}
/*Funcion que analiza la sintaxis de la declaracion de un procedimiento
 */
void Procedimiento()
{
	if(numero!=3)  //Si no es la palabra PROC sale de la funcion
		return;
	leetoken();
	if(numero==12) //verifica si es un Identificador
	{
		leetoken();
		if(numero==17)//verifica si es un ;
		{
			leetoken();
			Bloque(); //llama a la funcion de Bloque()
			if(numero!=17) //saliendo de Bloque() el token leido debe ser ;
				errores(20); //si no lo es hay error
			leetoken();
			Procedimiento(); //llama a la esta funcion en caso de haber mas procedimientos
		}
		else
		 errores(20);
	}
	else
	 errores(8);
}
/*Funcion de expre encargada de evaluar expresiones
 */
void Expre()
{
	Termino(); //llama a estas 2 funciones
	aux_e();
}

/*Funcion de Factor que verifica la sintaxis de una expresion
  con numeros, id y/o parentesis
 */
void Factor()
{
	if(numero==13 || numero==12) //cheka si es un id o numero
		leetoken();  //si lo es lee lo siguiente
	else if(numero==18) //verifica si es un (
	{
		leetoken(); //lee el siguiente token y llama a
		Expre(); //la funcion de expre
		if(numero!=19) //verifica si esta el parentesis de cierre )
			errores(11);
		leetoken();
	}
	else // si no fue ninguno de los anteriores: ID,NUM O (  ...error
		errores(24);
}

void aux_e()
{
	//Si es diferente del conjunto de sus follows
	if(numero!=23 && numero!=22 && numero!=26 && numero!=24 && numero!=27 && numero!=25 && numero!=21 && numero!=20 && numero!=19 && numero!=14 && numero!=17 && numero!=5)
	{
		if(numero==28 || numero==29) //verifica si es + o -
		{
			leetoken(); //si lo es lee lo siguiente y llama
			Expre();    //a la funcion expre
		}
	}
}

void Termino()
{
	Factor();
	aux_t();
}

void aux_t()
{
	//Si es diferente del conjunto de sus follows
	if(numero!=23 && numero!=22 && numero!=26 && numero!=24 && numero!=27 && numero!=25 && numero!=21 && numero!=20 && numero!=19 && numero!=14 && numero!=17 && numero!=5)
	{
		if(numero==30 || numero==31) //Verifica si es un * o /
		{
			leetoken();  //Si lo es lee lo siguiente y llama a expre
			Termino();
		}
	}
}

/*Funcion que verifica si el token actual es un operador logico
  ya sea < > <= ... etc
 */
void operadoresLogicos()
{
	//Si es diferente del conjunto de sus follows
	if(numero!=23 && numero!=22 && numero!=26 && numero!=24 && numero!=27 && numero!=25)
	{
		errores(5);
	}
	leetoken();
}

/*Funcion que engloba a las anteriores verificando primero una expresion
 luego un operador logico y porteriormente otra expresion
 por ejemplo  x+y <= a+b
*/
void Condicion()
{
	Expre();
	operadoresLogicos();
	Expre();
}

/*Funcion pricipal en la cual se mandan llamar a las propocisiones o
  sentencias dentro del codigo ya sea inicio, leer, escribir etc
  deacuerdo al numero actual del token
*/
void Propocisiones()
{
	switch(numero)
	{
		case 4: Inicio(); break;
		case 12: Declaracion(); break;
		case 6: Escribir(); break;
		case 7: Leer(); break;
		case 8: Llamar(); break;
		case 9: Si(); break;
		case 10: Mientras(); break;
		case 11: Para(); break;
		default: errores(22); break;
	}
}
/*Funcion que llama a la funcion de inicio2 para las proposiciones
 */
void Inicio()
{
	leetoken();
	inicio2();;
}

/*Funcion que verifica si hay inicios anidados
 */
void inicio2()
{
	//Si es diferente del conjunto de sus follows
	if(numero!=12 && numero!=4 && numero!=6 && numero!=7 && numero!=8 && numero!=9 && numero!=10 && numero!=11)
		errores(22);
	else
	{
		Propocisiones();  //Llama de nuevo a propociciones para en
		aux_inicio();     //caso de haber inicios anidados
	}

}
/*Funcion q verifica la sintaxis de una declaracion de Inicio
 */
void aux_inicio()
{
	if(numero!=5) //Verifica si no se ha llegado al fin
	{
		if(numero==17) //Verifica si es ; para sentencias anidadas en el mismo Inicio
		{
			leetoken();
			inicio2();
		}
		else if(flag==FALSE)//Verifica si ya habia un error de este tipo para no poner
		{                   //tantos errores como anidaciones de inicios haya encontrado
			errores(20);
			flag=TRUE;
		}
	}
	else
		leetoken();//Si ya se llego al token FIN lee el siguiente
}
/*Funcion que verifica la sintaxis de la sentencia de declaracion
  por ej  x=10
 */
void Declaracion()
{
	leetoken();
	if(numero!=15) //verifica si hay un =
		errores(14);
	else
	{
		leetoken(); //lee lo siguiente y llama a expre para
		Expre();    //la expresion de la declaracion
	}
}

/*Funcion que verifica que si despues de un escribir haya un numero
  o un identificador
 */
void Escribir()
{
	leetoken();
	if(numero!=13 && numero!=12)
		errores(7);
	leetoken();
}

/*Funcion que verifica cuando que despues de la sentencia LEER haya un ID
 */
void Leer()
{
	leetoken();
	if(numero!=12)
		errores(8);
	leetoken();
}
/*Funcion que verifica cuando que despues de la sentencia LLAMAR haya un ID
 */
void Llamar()
{
	leetoken();
	if(numero!=12)
		errores(8);
	leetoken();
}
/*Esta funcion verifica la sintacis de una sentencis tipo IF
 */
void Si()
{
	leetoken();
	if(numero!=18) //verifica si es un (
		errores(30);
	else
		leetoken();
	Condicion(); // llama a la funcion de condicion
	if(numero!=19) //verifica si hay un parentesis de cierre )
		errores(12);
	leetoken();      //lee lo siguiente y llama a la funcion
	Propocisiones(); //de propocisiones para la(s) sentencias del IF
}
/*Igual a la funcion anterior pero ahora para la sentencia MIENTREAS O WHILE
 */
void Mientras()
{
	leetoken();
	if(numero!=18)
		errores(30);
	else
		leetoken();
	Condicion();
	if(numero!=19)
		errores(12);
	leetoken();
	Propocisiones();
}
/*Funcion que hace la verificacion de una sentencia PARA
 */
void Para()
{
	leetoken();
	if(numero!=18) //verifica si es un (
		errores(11);
	else
	{
		leetoken();
		if(numero!=12) //verifica si hay un identificador
			errores(8);
		else
		{
			leetoken();
			if(numero!=15) //verifica si es un =
				errores(14);
			else
			{
				leetoken();
				Expre();  //llama a la funcion de expre
				if(numero==21 || numero==20)  //verifica si hay un & o %
				{
					leetoken();
					Expre();  // llama a la funcion expre para la declaracion del ID
					if(numero!=19)//verifica su hay parentesis de cierre )
						errores(12);
					else
					{
						leetoken();      //lee lo siguiente y llama a la funcion de propocision
						Propocisiones(); //para la(s) sentencias de PARA
					}
				}
				else
					errores(16);
			}
		}
	}
}

void main(int argc, char *args[])
{
	clrscr();
	if(argc==1)//En caso de no haber introducido correctamente los argumentos
	{
		printf("Uso incorrecto\nUso:\tcompila nombre_archivo.pl0");
		getch();
		exit(0);
	}
	strcpy(nomArchivo,objLex.obtenNombre(args[1]));
	objLex.leeDatos(args[1]);
	if(objLex.errorLectura!=1) //Si no hubo ningun error de lectura del archivo
	{
		if(errorLexico==FALSE) //Si no hay errores Lexicos hace el analisis sintactico
		{
			leetoken();
			Programa();
		}
		else //Si hay errores Lexicos Mostramos los tokens y los errores Lexicos
		{
			objLex.muestraTokens(args[1]);
		}
	}
}
