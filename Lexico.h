#ifndef LEXICO_H_INCLUDED
#define LEXICO_H_INCLUDED

#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>

using namespace std;

void print_list(vector<string> &vectorEntrada){

    for(int i = 0; i < vectorEntrada.size(); i++){
        cout<< "[" << vectorEntrada[i] << "]" << endl;
    }
}

void print_matriz(vector<vector<string> > &matriz){

    for(int i = 0; i < matriz.size(); i++){
        for(int j = 0; j < matriz[i].size(); j++){
            cout << matriz[i][j] << "  ";
        }
        cout << endl;
    }
}

bool isNumero(string cadena){
    int i = 0;

    while( i< cadena.size() ){
    if(cadena[i] >= 48 &&  cadena[i] <= 57){
        i++;
    }else{
        return 0; }
    }
    return 1;
}

int isFloat(string cadena){
    string aux,aux2; int j=-1;
    if(cadena[0] == '.')
      return 0;
    for (int i = 0 ; i< cadena.size(); i++){
      j=cadena.find('.');
      aux = cadena.substr (0,j);
      aux2 = cadena.substr (j+1,cadena.size()-(j+1));
      if(isNumero(aux) && isNumero(aux2))
          return 1;

      else {
        return 0;
      }

    }

}

int	isCadena(string cadena){
    int i = 0 ;
    int flag=0;
    while (i< cadena.size() ) {
        if(cadena[i] >=65 &&  cadena[i]<= 90 || cadena[i]>=97 && cadena [i]<=122 || cadena[i] >= 48 &&  cadena[i] <= 57
          || cadena[i] == 95)
        i++;
        else {
            return 0;
        }
    } return 1;
}

vector<string> split2(string str, char delimiter){

    vector<string> internal;
    stringstream ss(str);
    string tok;
    string tok2;
    string sep = ";";
    int pos = 0;
    if (str.empty())
    {
      return internal;
    }
    else{
      while(getline(ss, tok, delimiter)) {
        stringstream s1(tok);
        pos = tok.find(';');
        if (pos > 0)
        {
          getline(s1,tok2,';');
          internal.push_back(tok2);
          internal.push_back(sep);
        }
        else
        {
          internal.push_back(tok);
        }
      }

    }

    return internal;
}

vector<int> minimo(string sep, string aux)
{
  vector<int> resultado;
  int j = -1,pos=0;
  int min = 1000;
  for(int i = 0;i<sep.size();i++)
  {
    pos = aux.find(sep[i]);
    if(min>pos && pos>-1)
    {
      min = pos;
      j = i;
    }

  }
  resultado.push_back(j);
  resultado.push_back(min);
  return resultado;
}


vector<string> split(string str, char delimiter){

    vector<string> internal;
    vector<int>resultado;
    stringstream ss(str);
    string tok;
    string tok2;
    string sepa;
    string aux;
    string sep = "=()[]{}><+-,;*/!&|!";
    int pos = 0;
    while(getline(ss, tok, delimiter)) {
        stringstream s1(tok);
        aux = tok;
        while(aux.length()!=0)
        {
          resultado=minimo(sep,aux);
          if (aux[0] == 34)
          {
            string aux2 = aux.substr(0,aux.size()-1);
            internal.push_back(aux2);
            aux = aux.substr (aux.size()-1,aux.size());
            s1.str(aux);
          }
          else if(resultado[1]!=1000 && resultado[0]!=-1)
          {
            sepa = sep[resultado[0]];
            getline(s1,tok2,sep[resultado[0]]);
            aux = aux.substr (resultado[1]+1,aux.size());

            if(tok2.length()==0 && aux !=sepa)
            {
              internal.push_back(sepa);
            }
            else if(aux == sepa)
            {
              if(tok2.length()>0)
              {
                internal.push_back(tok2);
              }
              internal.push_back(sepa);
              aux = "";
            }
            else
            {
              internal.push_back(tok2);
              internal.push_back(sepa);
            }
          }
          else
          {
            if(aux.size()>=1)
            {
              internal.push_back(aux);
            }
            else
            {
              internal.push_back(tok);
            }
            aux = "";
            pos=-1;

          }
        }

    }
    return internal;
}


class Lexico{

    public:

    string cadena;
    int nroLinea;
    string token;
    vector<string> separados;
    vector<string> comentarios;
    vector<string> separadosTokens;
    vector<vector<string> > matriz;
    bool flagFinLinea = false;

    Lexico(){
        cadena = "";
        token = "";
        nroLinea = 1;
    }

    void procesar(){
        ifstream codigoEntrada("codigo.txt");
        string lineaCodigo = " ";

        if(codigoEntrada.is_open()){
            while(getline(codigoEntrada, lineaCodigo)){
                if (lineaCodigo[0] == '#'){
                    comentarios.push_back(lineaCodigo);
                    continue;
                }
                else{
                    transform(lineaCodigo.begin(), lineaCodigo.end(), lineaCodigo.begin(), ::tolower);
                    separados = split(lineaCodigo,' ');
                    matriz.push_back(separados);
                }
            }
            codigoEntrada.close();
        }else{
            cout << "No se pudo abrir el archivo";
        }

        ofstream archivoTokens;
        archivoTokens.open("listaTokens.txt");

        for(int i = 0; i < matriz.size(); i++){
            archivoTokens<<"-------------INICIO LINEA "<< i+1 << "-------------"<<endl;
            for(int j = 0; j < matriz[i].size(); j++){
                string tmp = matriz[i][j];
                string tmp1;
              if (tmp == "int" || tmp == "float" || tmp == "string" || tmp == "char" || tmp == "void"){
                separadosTokens.push_back("TIP_DAT");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : TIP_DAT \t";
                archivoTokens<<  ",TOKEN: "<<tmp <<endl;
              }
              else if (tmp == "*" || tmp == "-" || tmp == "+" || tmp == "/" || tmp == "%"){
                tmp1 = matriz[i][j+1];
                if(tmp1 == "+" || tmp1 == "-")
                {
                  separadosTokens.push_back("OPE_INCRE");
                  archivoTokens << nroLinea++ << "\t";
                  archivoTokens<< "LEXEMA : OPE_INCRE \t";
                  archivoTokens<<  ",TOKEN: "<< tmp+tmp1 <<endl;
                  j++;
                }
                else
                {
                  separadosTokens.push_back("OPE_ARIT");
                  archivoTokens << nroLinea++ << "\t";
                  archivoTokens<< "LEXEMA : OPE_ARIT \t";
                  archivoTokens<<  ",TOKEN: "<< tmp <<endl;
                }

              }
              else if (tmp == "<" || tmp == ">" || tmp == "!"){
                tmp1 = matriz[i][j+1];
                  if (tmp1 == "="){
                  separadosTokens.push_back("OPE_CMP");
                  archivoTokens << nroLinea++ << "\t";
                  archivoTokens<< "LEXEMA : OPE_CMP \t";
                  archivoTokens<<  ",TOKEN: "<< tmp+tmp1 <<endl;
                  j++;
                }
                else
                {
                  separadosTokens.push_back("OPE_CMP");
                  archivoTokens << nroLinea++ << "\t";
                  archivoTokens<< "LEXEMA : OPE_CMP \t";
                  archivoTokens<<  ",TOKEN: "<< tmp <<endl;
                }

              }

              else if (tmp == "and" || tmp == "or"){
                separadosTokens.push_back("OPE_LOG");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : OPE_LOG \t" ;
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "main"){
                separadosTokens.push_back("MAIN");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : MAIN \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "return"){
                separadosTokens.push_back("RETURN");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : RETURN \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "break"){
                separadosTokens.push_back("BREAK");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : BREAK \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "#"){
                separadosTokens.push_back("MACRO");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : MACRO \t" ;
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp[0] == 34 && tmp[tmp.size() -1] == 34){
                separadosTokens.push_back("STRING");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : STRING \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp[0] == 39 && tmp[tmp.size() -1] == 39){
                separadosTokens.push_back("CHAR");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : CHAR \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "("){
                separadosTokens.push_back("PAR_OP");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : PAR_OP \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp[0]==')'){
                separadosTokens.push_back("PAR_CL");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : PAR_CL \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp[0]=='{'){
                separadosTokens.push_back("LL_OP");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : LL_OP \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp[0]=='}'){
                separadosTokens.push_back("LL_CL");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : LL_CL \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "["){
                separadosTokens.push_back("COR_OP");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : COR_OP \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "]"){
                separadosTokens.push_back("COR_CL");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : COR_CL \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == ","){
                separadosTokens.push_back("COMA");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : COMA \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == ";"){
                separadosTokens.push_back("FINL");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : FINL \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
                flagFinLinea=1;
              }
              else if (tmp == "="){
                tmp1 = matriz[i][j+1];
                  if (tmp1 == "="){
                  separadosTokens.push_back("OPE_CMP");
                  archivoTokens << nroLinea++ << "\t";
                  archivoTokens<< "LEXEMA : OPE_CMP \t";
                  archivoTokens<<  ",TOKEN: "<< tmp+tmp1 <<endl;
                  j++;
                }
                else
                {
                  separadosTokens.push_back("IGUAL");
                  archivoTokens << nroLinea++ << "\t";
                  archivoTokens<< "LEXEMA : IGUAL \t";
                  archivoTokens<<  ",TOKEN: "<< tmp <<endl;
                }

              }
              else if (tmp == "if"){
                separadosTokens.push_back("IF");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : IF \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "else"){
                separadosTokens.push_back("ELSE");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : ELSE \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "elif"){
                separadosTokens.push_back("ELSEIF");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : ELSEIF \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "while"){
                separadosTokens.push_back("WHILE");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : WHILE \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == "for"){
                separadosTokens.push_back("FOR");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : FOR \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (tmp == " " || tmp == "\t" || tmp == "\n" || tmp == ""){
                j++;
              }
              else if (isNumero(tmp)){
                separadosTokens.push_back("NUMERO_ENTERO");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : NUMERO_ENTERO \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (isFloat(tmp)){
                separadosTokens.push_back("NUMERO_FlOTANTE");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : NUMERO_FlOTANTE \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else if (isCadena(tmp)){
                separadosTokens.push_back("NOM_VAR");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : NOM_VAR \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
              else {
                separadosTokens.push_back("ERROR");
                archivoTokens << nroLinea++ << "\t";
                archivoTokens<< "LEXEMA : ERROR \t";
                archivoTokens<<  ",TOKEN: "<< tmp <<endl;
              }
            }
        }

        archivoTokens.close();
    }
};

#endif // LEXICO_H_INCLUDED
